package com.company;

class Main {

    /**
     * metoda main powinna implementowac algorytm do
     * jak najszybszego wyszukiwania wartosci
     * zmiennej digit z klasy QuizImpl (zakladamy ze
     * programista nie zna zawartosci klasy QuizImpl).
     * Nalezy zalozyc, ze pole digit w klasie QuizImpl
     * moze w kazdej chwili ulec zmianie. Do wyszukiwania
     * odpowiedniej wartosci nalezy wykorzystywac tylko
     * i wylacznie metode isCorrectValue - jesli metoda
     * przestanie rzucac wyjatki wowczas mamy pewnosc ze
     * poszukiwana zmienna zostala odnaleziona.
     */
    public static void main(String[] args) {

        Quiz quiz = new QuizImpl();
        int digit = Quiz.MAX_VALUE/2 ; // zainicjuj zmienna
        int minDigit = Quiz.MIN_VALUE;
        int maxDigit = Quiz.MAX_VALUE;

        for (int counter = 1; ; counter++) {

            try {
                System.out.println(digit);
                quiz.isCorrectValue(digit);
                System.out.println("Trafiona proba!!! Szukana liczba to: "
                        + digit + " Ilosc prob: " + counter);
                break;

            } catch (Quiz.ParamTooLarge paramTooLarge) {

                System.out.println("Argument za duzy!!!");
                maxDigit=digit;
                digit=(minDigit+maxDigit)/2;
                // implementacja logiki...

            } catch (Quiz.ParamTooSmall paramTooSmall) {

                System.out.println("Argument za maly!!!");
                minDigit=digit;
                digit=(minDigit+maxDigit)/2;
                // implementacja logiki...
            }
        }
    }
}

